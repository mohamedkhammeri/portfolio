import { TestBed } from '@angular/core/testing';

import { PdfEmbeddedService } from './pdf-embedded.service';

describe('PdfEmbeddedService', () => {
  let service: PdfEmbeddedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PdfEmbeddedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
