import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {
  projects: any = [];
  hasProject: Subject<any> = new BehaviorSubject(undefined);

  constructor(private httpClient: HttpClient) {
    this.fetchProjects()
      .subscribe(_data => this.projects = _data);
  }

  fetchProjects() {
    return this.httpClient.get('/assets/data/projects.json');
  }

  findByKey(key: string): any {
    if (this.projects.length == 0) {
      this.fetchProjects()
        .subscribe(_data => {
          this.projects = _data;
          this.hasProject.next(this.projects.find((_project: any) => _project.key == key))
        });
      return this.hasProject;
    }
    return this.projects.find((_project: any) => _project.key == key);
  }
}
