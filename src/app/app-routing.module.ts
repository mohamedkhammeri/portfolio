import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProjectDetailsComponent} from "./components/project-details/project-details.component";
import {MainComponent} from "./components/main/main.component";

const routes: Routes = [
  {path: "details/:key", component: ProjectDetailsComponent},
  {path: "", component: MainComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
