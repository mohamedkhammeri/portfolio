import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/header/header.component';
import { HeroComponent } from './components/hero/hero.component';
import { MainComponent } from './components/main/main.component';
import { AboutComponent } from './components/about/about.component';
import { FactsComponent } from './components/facts/facts.component';
import { SkillsComponent } from './components/skills/skills.component';
import { ResumeComponent } from './components/resume/resume.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { ContactComponent } from './components/contact/contact.component';
import { FooterComponent } from './components/footer/footer.component';
import {LightboxModule} from "ngx-lightbox";
import { ProjectDetailsComponent } from './components/project-details/project-details.component';
import {ProjectsService} from "./services/projects.service";
import {SwiperModule} from "swiper/angular";
import {PdfEmbeddedService} from "./services/pdf-embedded.service";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HeroComponent,
    MainComponent,
    AboutComponent,
    FactsComponent,
    SkillsComponent,
    ResumeComponent,
    PortfolioComponent,
    ContactComponent,
    FooterComponent,
    ProjectDetailsComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        LightboxModule,
        SwiperModule,
        ReactiveFormsModule
    ],
  providers: [
    ProjectsService,
    PdfEmbeddedService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
