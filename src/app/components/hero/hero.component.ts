import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
// @ts-ignore
import Typed from 'typed.js/lib/typed.js';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements AfterViewInit {

  @ViewChild("typedEl")
  typedEl?: ElementRef;

  strings: string[] = ['Computer Science engineer', 'Java/PHP Developer', 'Junior Business Analyst' , ];

  constructor() {
  }

  ngAfterViewInit(): void {
    new Typed('.typed', {
      strings: this.strings,
      loop: true,
      typeSpeed: 100,
      backSpeed: 50,
      backDelay: 2000,
      shuffle: true
    });
  }

}
