import {Component, OnInit} from '@angular/core';
import {Lightbox} from "ngx-lightbox";

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  resumes: any = [];

  constructor(private _lightbox: Lightbox) {
  }

  ngOnInit(): void {
    document.querySelectorAll('a[data-gallery="portfolioGallery"]')
      .forEach((_el, index) => {
        const imgSrc = _el.attributes.getNamedItem('href')?.nodeValue;
        this.resumes.push({src: imgSrc, thumb: imgSrc})
        _el.addEventListener('click', (e) => {
          e.preventDefault();
          this.open(index);
        });
      })
  }

  open(index: number) {
    this._lightbox.open(this.resumes, index);
  }
}
