import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import 'waypoints/lib/noframework.waypoints';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements AfterViewInit {

  @ViewChild("skillsEl")
  skillsEl?: ElementRef;

  constructor() {
  }

  ngAfterViewInit(): void {
    const skEl = this.skillsEl?.nativeElement;
    // @ts-ignore
    new Waypoint({
      element: skEl,
      offset: '80%',
      handler: function () {
        let progress = document.querySelectorAll('.progress .progress-bar');
        progress.forEach((el: any) => {
          el.style.width = el.getAttribute('aria-valuenow') + '%'
        });
      }
    })
  }

}
