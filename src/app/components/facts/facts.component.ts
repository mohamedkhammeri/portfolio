import {AfterViewInit, Component} from '@angular/core';
import PureCounter from '@srexi/purecounterjs/js/purecounter';

@Component({
  selector: 'app-facts',
  templateUrl: './facts.component.html',
  styleUrls: ['./facts.component.scss']
})
export class FactsComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit(): void {
    new PureCounter;
  }

}
