import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ProjectsService} from "../../services/projects.service";
import {Observable} from "rxjs";
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y, Autoplay } from 'swiper';

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Autoplay]);

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit {
  key?: string;
  project: any;
  swiperConfig: any;

  constructor(private router: ActivatedRoute,
              private projectService: ProjectsService) {
    router.params.subscribe(_params => {
      this.key = _params.key;

      if (this.key) {
        const result = this.projectService.findByKey(this.key);
        if (result instanceof Observable) {
          result.subscribe(_data => {
            this.project = _data;
            setTimeout(() => {
              this.scrollTo("main");
            }, 200);
          });
        } else {
          this.project = result;
          this.scrollTo("main");
        }
      }
    });

    this.swiperConfig = {
      speed: 400,
      loop: true,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false
      },
      spaceBetween: 50,
      navigation: true,
      pagination: { clickable: true },
      scrollbar: { draggable: true },
    }
  }

  ngOnInit(): void {
    this.router.fragment.subscribe(f => {
      this.scrollTo(f);
    });
  }

  scrollTo(fragment: any) {
    const el = document.querySelector("#" + fragment);
    el?.scrollIntoView();
  }

}
