import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PdfEmbeddedService} from "../../services/pdf-embedded.service";


@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss']
})
export class ResumeComponent implements OnInit, AfterViewInit {
  private config: any = {
    showAnnotationTools: false, showLeftHandPanel: false, showPageControls: false,
    showPrintPDF: false
  };

  @ViewChild("pdf")
  pdf?: ElementRef;

  @ViewChild("fullImg")
  fullImg?: ElementRef;

  constructor(private readonly pdfService: PdfEmbeddedService) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    /* this.pdfService.ready()
       .then(() => {
         //1
         this.pdfService.previewFile(
           "thomas-pdf", //div id
           this.config,
           "assets/img/MohamedKhammeriNotificationLetter.pdf" //file path
         );
         //others

       });*/
  }

  openModal(_event: MouseEvent): void {
    if (!this.fullImg) {
      return;
    }
    const modalImg: any = document.getElementById("img01")
    const captionText: any = document.getElementById("caption")
    if (modalImg && captionText) {
      const toggle: any = document.querySelector("i.mobile-nav-toggle");
      toggle.style.zIndex = "unset";
      this.fullImg.nativeElement.style.display = "block";
      const el: any = _event.target;
      modalImg.src = el?.src;
      captionText.innerHTML = el?.alt;
    }
  }

  closeModal() {
    if (!this.fullImg) {
      return;
    }
    this.fullImg.nativeElement.style.display = "none";
    const toggle: any = document.querySelector("i.mobile-nav-toggle");
    toggle.style.zIndex = "";
    const modalImg = document.getElementById("img01");
    if (modalImg) { // @ts-ignore
      delete modalImg.src;
    }
  }
}
