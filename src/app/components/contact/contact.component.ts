import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
// @ts-ignore
import * as L from "leaflet/dist/leaflet";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit, AfterViewInit {

  @ViewChild("map")
  map?: ElementRef;
  lat = 36.9025;
  lon = 10.1826;

  baseContact: string = "mailto:Moe.khammeri@gmail.com";
  contactPayload: string = "";
  contactForm?: FormGroup;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.contactForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      subject: new FormControl('', [Validators.required]),
      message: new FormControl('', [Validators.required]),
    });

    this.contactForm.valueChanges.subscribe(_values => {
      this.contactPayload = `${this.baseContact}?subject=${_values.subject}&body=${_values.message}`;
    })
  }

  /*
  @deprecated
   */
  initMap() {
    // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
    const m = L.map(this.map?.nativeElement).setView([this.lat, this.lon], 15);

    const latlng = new L.LatLng(this.lat, this.lon);
    let center = m.project(latlng);
    center = new L.point(center.x - 150, center.y - 100);
    const target = m.unproject(center);
    m.panTo(target);
    // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
    L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
      // Il est toujours bien de laisser le lien vers la source des données
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      minZoom: 1,
      maxZoom: 20,
    }).addTo(m);

    L.marker([this.lat, this.lon]).addTo(m);
    m.invalidateSize();
  }

}
