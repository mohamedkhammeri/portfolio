// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAgxMeVRkQx9lTWhyMHJMarqlbh23_aEjw",
    authDomain: "portfolio-medkhammeri.firebaseapp.com",
    projectId: "portfolio-medkhammeri",
    storageBucket: "portfolio-medkhammeri.appspot.com",
    messagingSenderId: "800612329775",
    appId: "1:800612329775:web:2422e8c5a8c9de83a8d9da",
    measurementId: "G-5VZZ6MGW09"
  },
  adobeClientId: 'c06a5446cc5642b2a535237a1fea2fa6'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
